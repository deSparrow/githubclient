//
//  FavoritesPresenter.swift
//  GitHubClient
//
//  Created by Fabe on 30/08/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class FavoritesPresenter {
    
    private let coreDataManager: CoreDataManager
    weak private var favoritesView: FavoritesView?
    private var repositories: [RepositoryDM] = []
    
    init(coreDataManager: CoreDataManager) {
        self.coreDataManager = coreDataManager
    }
    
    func injectView(view: FavoritesView) {
        favoritesView = view
    }
    
    func getRepositoriesCount() -> Int {
        return repositories.count
    }
    
    func getRepositories() -> [RepositoryDM] {
        return repositories
    }
    
    func viewLoaded() {
        let repos = coreDataManager.fetch(RepositoryDBM.self)
        repositories = repos.map({ dbm -> RepositoryDM in
            let dm = RepositoryDM(repositoryName: dbm.name ?? "Name", repositoryDescription: dbm.desc ?? RepositoryDefaults.description, openIssuesCount: Int(dbm.openIssuesCount), starsCount: Int(dbm.starsCount), forksCount: Int(dbm.forksCount), language: dbm.language ?? RepositoryDefaults.language, username: dbm.ownerName ?? UserDefaults.name)
            return dm
        }).reversed()
        favoritesView?.showFavoriteRepositories()
    }
    
    func deletedRepository(at row: Int) {
        let repository = repositories[row]
        let persistedRepo = coreDataManager.fetch(RepositoryDBM.self).first { $0.name == repository.repositoryName }
        
        guard let repo = persistedRepo else {
            return
        }
        
        coreDataManager.context.delete(repo)
        coreDataManager.saveContext { [weak self] error in
            guard let this = self else {
                return
            }
            if let error = error {
                NSLog(error.localizedDescription)
                return
            }
            this.repositories.remove(at: row)
            this.favoritesView?.removeRepository(at: row)
        }
    }
}
