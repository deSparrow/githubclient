//
//  RepositoryDetailPresenter.swift
//  GitHubClient
//
//  Created by Fabe on 12/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class RepositoryDetailPresenter {
    
    private let gitHubUserDataSource: GitHubUserDataSource
    private let coreDataManager: CoreDataManager
    weak private var repositoryDetailView: RepositoryDetailView?
    private var gitHubUser: UserDetailDM?
    var selectedRepositoryDM: RepositoryDM?
    
    init(gitHubUserDS: GitHubUserDataSource, coreDataManager: CoreDataManager) {
        gitHubUserDataSource = gitHubUserDS
        self.coreDataManager = coreDataManager
    }
    
    func inject(view: RepositoryDetailView) {
        repositoryDetailView = view
    }
    
    func viewLoaded() {
        guard let repository = selectedRepositoryDM else {
            return
        }
        
        //Fill view with available data and fetch GitHub user
        repositoryDetailView?.fillRepositoryInfo(description: repository.repositoryDescription, language: repository.language)
        gitHubUserDataSource.getGitHubUserForLogin(login: repository.username) { [weak self] userDetailDM, error in
            guard let this = self else {
                return
            }
            
            if let user = userDetailDM {
                this.gitHubUser = user
                this.repositoryDetailView?.fillUserInfo(user: user)
            } else {
                this.repositoryDetailView?.fillFailedUserInfo(login: this.selectedRepositoryDM?.username ?? UserDefaults.name)
                this.repositoryDetailView?.displayErrorAlert("Error", "Couldn't fetch informations for repository owner")
            }
        }
        
        //Check if user is already in memory, if so, disable save button
        let repos = coreDataManager.fetch(RepositoryDBM.self)
        let persistedRepo = repos.first { dbm -> Bool in
            dbm.name == repository.repositoryName
        }
        if persistedRepo != nil {
            repositoryDetailView?.transformSaveRepositoryButton()
        }
    }
    
    func saveRepositoryButtonTriggered() {
        let repositoryDBM = RepositoryDBM(context: coreDataManager.context)
        repositoryDBM.name = selectedRepositoryDM?.repositoryName
        repositoryDBM.desc = selectedRepositoryDM?.repositoryDescription
        repositoryDBM.language = selectedRepositoryDM?.language
        repositoryDBM.starsCount = Int32(selectedRepositoryDM?.starsCount ?? 0)
        repositoryDBM.openIssuesCount = Int32(selectedRepositoryDM?.openIssuesCount ?? 0)
        repositoryDBM.forksCount = Int32(selectedRepositoryDM?.forksCount ?? 0)
        repositoryDBM.ownerName = gitHubUser?.name
        repositoryDBM.location = gitHubUser?.location
        repositoryDBM.address = gitHubUser?.email
        repositoryDBM.imageUrl = gitHubUser?.avatarURL?.absoluteString
        
        coreDataManager.saveContext() { [weak self] (error) in
            guard let this = self else {
                return
            }
            
            if let error = error {
                this.repositoryDetailView?.displayErrorAlert("Error", error.localizedDescription)
            } else {
                this.repositoryDetailView?.transformSaveRepositoryButton()
            }
            
        }
    }
}
