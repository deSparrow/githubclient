//
//  RepositoriesPresenter.swift
//  GitHubClient
//
//  Created by Fabe on 12/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class RepositoriesPresenter {
    
    private let repositoryDataSource: RepositoryDataSource
    weak private var repositoriesView: RepositoriesView?
    private var repositories: [RepositoryDM] = []
    
    init(repositoryDS: RepositoryDataSource) {
        repositoryDataSource = repositoryDS
    }
    
    func injectView(view: RepositoriesView) {
        repositoriesView = view
    }
    
    func getRepositoriesCount() -> Int {
        return repositories.count
    }
    
    func getRepositories() -> [RepositoryDM] {
        return repositories
    }
    
    func searchBarTriggered(with text: String) {
        repositories.removeAll()
        repositoriesView?.refreshRepositories()
        repositoriesView?.showActivityIndicator()
        repositoryDataSource.getRepositoriesForUserName(userName: text, limit: 20, completion: { [weak self] repositories, error in
            guard let this = self else {
                return
            }
            
            this.repositoriesView?.hideActivityIndicator()
            
            guard let error = error else {
                this.repositories = repositories ?? []
                this.repositoriesView?.refreshRepositories()
                return
            }
            
            this.repositories = []
            var title = "User not found"
            var message = "GitHub doesn't know that username"
            if let generalError = error as? GeneralError {
                title = "Error"
                if case .error(let msg) = generalError {
                    message = msg
                } else {
                    message = "Unknown error has occured"
                }
            }
            this.repositoriesView?.displayErrorAlert(title, message)
            this.repositoriesView?.refreshRepositories()
        })
    }
}
