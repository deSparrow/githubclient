//
//  RepositoryDetailView.swift
//  GitHubClient
//
//  Created by Fabe on 12/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol RepositoryDetailView: class {
    func fillRepositoryInfo(description: String, language: String)
    func fillUserInfo(user: UserDetailDM)
    func fillFailedUserInfo(login: String)
    func displayErrorAlert(_ title: String, _ message: String)
    func transformSaveRepositoryButton()
}
