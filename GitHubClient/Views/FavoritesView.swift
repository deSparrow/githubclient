//
//  FavoritesView.swift
//  GitHubClient
//
//  Created by Fabe on 30/08/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol FavoritesView: class {
    func showFavoriteRepositories()
    func removeRepository(at row: Int)
}
