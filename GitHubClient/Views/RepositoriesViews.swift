//
//  RepositoriesViews.swift
//  GitHubClient
//
//  Created by Fabe on 12/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol RepositoriesView: class {
    func refreshRepositories()
    func displayErrorAlert(_ title: String, _ message: String)
    func showActivityIndicator()
    func hideActivityIndicator()
}
