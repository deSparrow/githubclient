//
//  UserDetailDM.swift
//  GitHubClient
//
//  Created by Fabe on 10/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class UserDetailDM: DMType {
    var login: String
    var avatarURL: URL?
    var name: String
    var location: String
    var email: String
    
    init(userLogin: String, userAvatar: String?, userName: String, userLocation: String, userEmail: String) {
        login = userLogin
        if let avatarLink = userAvatar, let url = URL(string: avatarLink) {
            avatarURL = url
        }
        name = userName
        location = userLocation
        email = userEmail
    }
}
