//
//  RepositoryDM.swift
//  GitHubClient
//
//  Created by Fabe on 09/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class RepositoryDM: DMType {
    var repositoryName: String
    var repositoryDescription: String
    var openIssuesCount: Int
    var starsCount: Int
    var forksCount: Int
    var language: String
    var username: String
    
    init(repositoryName: String, repositoryDescription: String, openIssuesCount: Int, starsCount: Int, forksCount: Int, language: String, username: String) {
        self.repositoryName = repositoryName
        self.repositoryDescription = repositoryDescription
        self.openIssuesCount = openIssuesCount
        self.starsCount = starsCount
        self.forksCount = forksCount
        self.language = language
        self.username = username
    }
}
