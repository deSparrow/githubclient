//
//  RepositoriesViewController.swift
//  GitHubClient
//
//  Created by Fabe on 09/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var repositoriesTableView: UITableView!
    @IBOutlet weak var repositorySearchBar: UISearchBar!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    private let presenter = RepositoriesPresenter(repositoryDS: RepositoryServiceManager())

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.injectView(view: self)
        repositoriesTableView.delegate = self
        repositoriesTableView.dataSource = self
        repositorySearchBar.delegate = self
        repositoriesTableView.keyboardDismissMode = .onDrag
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getRepositoriesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryCell
        
        let repo = presenter.getRepositories()[indexPath.row]
        cell.repositoryName.text = repo.repositoryName
        cell.repositoryDescription.text = repo.repositoryDescription
        cell.openIssuesCount.text = repo.openIssuesCount.toString()
        cell.starsCount.text = repo.starsCount.toString()
        cell.forksCount.text = repo.forksCount.toString()
        
        return cell
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty else {
            return
        }
        searchBar.endEditing(true)
        presenter.searchBarTriggered(with: text)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRepositoryDetail" {
            let repositoryDetailVC = segue.destination as! RepositoryDetailViewController
            if let row = repositoriesTableView.indexPathForSelectedRow?.row {
                repositoryDetailVC.presenter.selectedRepositoryDM = presenter.getRepositories()[row]
            }
            let deleteRepoButton = UIBarButtonItem(title: "Save", style: .plain, target: repositoryDetailVC.self, action: Selector("saveRepositoryButtonClicked"))
            repositoryDetailVC.navigationItem.rightBarButtonItem  = deleteRepoButton
        }
    }

}

extension RepositoriesViewController: RepositoriesView {
    
    func refreshRepositories() {
        repositoriesTableView.reloadData()
    }
    
    func displayErrorAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        present(alert, animated: true)
    }
    
    func showActivityIndicator() {
        activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityIndicatorView.stopAnimating()
    }
}
