//
//  FavoritesViewController.swift
//  GitHubClient
//
//  Created by Fabe on 30/08/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var repositoriesTableView: UITableView!
    
    private let presenter = FavoritesPresenter(coreDataManager: CoreDataManager.sharedInstance)

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.injectView(view: self)
        repositoriesTableView.delegate = self
        repositoriesTableView.dataSource = self
        presenter.viewLoaded()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getRepositoriesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryCell
        
        let repo = presenter.getRepositories()[indexPath.row]
        cell.repositoryName.text = repo.repositoryName
        cell.repositoryDescription.text = repo.repositoryDescription
        cell.openIssuesCount.text = repo.openIssuesCount.toString()
        cell.starsCount.text = repo.starsCount.toString()
        cell.forksCount.text = repo.forksCount.toString()
        
        return cell
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRepositoryDetail" {
            let repositoryDetailVC = segue.destination as! RepositoryDetailViewController
            if let row = repositoriesTableView.indexPathForSelectedRow?.row {
                repositoryDetailVC.presenter.selectedRepositoryDM = presenter.getRepositories()[row]
            }
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            presenter.deletedRepository(at: indexPath.row)
        }
    }
}

extension FavoritesViewController: FavoritesView {
    
    func showFavoriteRepositories() {
        repositoriesTableView.reloadData()
    }
    
    func removeRepository(at row: Int) {
        repositoriesTableView.beginUpdates()
        repositoriesTableView.deleteRows(at: [IndexPath(row: row, section: 0)], with: .fade)
        repositoriesTableView.endUpdates()
    }
    
}
