//
//  RepositoryDetailViewController.swift
//  GitHubClient
//
//  Created by Fabe on 09/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import UIKit

class RepositoryDetailViewController: UIViewController {
    
    @IBOutlet weak var repositoryDescription: UILabel!
    @IBOutlet weak var languageType: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var owner: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var location: UILabel!
    
    let presenter = RepositoryDetailPresenter(gitHubUserDS: GitHubUserServiceManager(), coreDataManager: CoreDataManager.sharedInstance)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.inject(view: self)
        presenter.viewLoaded()
    }
    
    @objc func saveRepositoryButtonClicked(){
        presenter.saveRepositoryButtonTriggered()
    }
    
}

extension RepositoryDetailViewController: RepositoryDetailView {
    func fillRepositoryInfo(description: String, language: String) {
        repositoryDescription.text = description
        languageType.text = language
    }
    
    func fillUserInfo(user: UserDetailDM) {
        owner.text = user.name
        email.text = user.email
        location.text = user.location
        avatar.load(from: user.avatarURL)
    }
    
    func fillFailedUserInfo(login: String) {
        owner.text = login
        email.text = UserDefaults.email
        location.text = UserDefaults.location
    }
    
    func displayErrorAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        present(alert, animated: true)
    }
    
    func transformSaveRepositoryButton() {
        guard let saveButton = self.navigationItem.rightBarButtonItem else {
            return
        }
        saveButton.isEnabled = false
    }
}
