//
//  GitHubUserServiceManager.swift
//  GitHubClient
//
//  Created by Fabe on 12/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol GitHubUserDataSource {
    func getGitHubUserForLogin(login: String, completion: @escaping (UserDetailDM?, Error?) -> Void)
}

class GitHubUserServiceManager: GitHubUserDataSource {
    
    fileprivate let endpoint = "https://api.github.com/users/"
    
    let networkManager = NetworkManager()
    
    func getGitHubUserForLogin(login: String, completion: @escaping (UserDetailDM?, Error?) -> Void) {
        let path = endpoint + login
        networkManager.doGet(path, completion: { jsonData, error in
            if let error = error {
                completion(nil, error)
            } else if let data = jsonData,
                let userDetailSM = try? JSONDecoder().decode(UserDetailSM.self, from: data) {
                completion(userDetailSM.toDM() as? UserDetailDM, nil)
            } else {
                completion(nil, GeneralError.error(msg: "Could not parse data to JSON"))
            }
        })
    }
}
