//
//  RepositoryServiceManager.swift
//  GitHubClient
//
//  Created by Fabe on 11/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol RepositoryDataSource {
    func getRepositoriesForUserName(userName: String, limit: Int, completion: @escaping ([RepositoryDM]?, Error?) -> Void)
}

class RepositoryServiceManager: RepositoryDataSource {
    
    fileprivate let endpoint = "https://api.github.com/search/repositories?"
    fileprivate let searchQuery = "user:"
    fileprivate let query = "q"
    fileprivate let perPage = "per_page"
    
    let networkManager = NetworkManager()
    
    func getRepositoriesForUserName(userName: String, limit: Int, completion: @escaping ([RepositoryDM]?, Error?) -> Void) {
        networkManager.doGet(endpoint, parameters: [query: searchQuery+userName, perPage: limit.toString()], completion: { jsonData, error in
            if let error = error {
                completion(nil, error)
            } else if let data = jsonData,
                let repository = try? JSONDecoder().decode(RepositorySM.self, from: data) {
                var repositories: [RepositoryDM] = []
                for repo in repository.repositories {
                    repositories.append(repo.toDM() as! RepositoryDM)
                }
                completion(repositories, nil)
            } else {
                completion(nil, GeneralError.error(msg: "Could not parse data to JSON"))
            }
        })
    }
}
