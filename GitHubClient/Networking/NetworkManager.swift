//
//  NetworkManager.swift
//  GitHubClient
//
//  Created by Fabe on 11/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation
import Alamofire

typealias NetworkCompletionHandler = (Data?, Error?) -> Void

protocol IHTTPMethod {
    func doGet(_ endpoint: String, parameters: [String: String], headers: [String: String], completion: @escaping NetworkCompletionHandler)
    func doPost(_ endpoint: String, headers: [String: String], body: Data?, completion: @escaping NetworkCompletionHandler)
}

class NetworkManager: IHTTPMethod {
    
    private func doRequest(_ urlRequest: URLRequest, completion: @escaping NetworkCompletionHandler) {
        Alamofire.request(urlRequest).validate().responseJSON { response in
            if let error = response.error {
                completion(nil, error)
            } else if let data = response.data {
                completion(data, nil)
            } else {
                completion(nil, GeneralError.unknownError)
            }
        }
    }
    
    fileprivate func addHeaders(urlRequest: inout URLRequest, headers: [String: String]) {
        for header in headers {
            urlRequest.addValue(header.value, forHTTPHeaderField: header.key)
        }
    }
    
    func doGet(_ endpoint: String, parameters: [String: String] = [:], headers: [String: String] = [:], completion: @escaping NetworkCompletionHandler) {
        var urlCompontents = URLComponents(string: endpoint)
        for parameter in parameters {
            urlCompontents?.queryItems?.append(URLQueryItem(name: parameter.key, value: parameter.value))
        }
        
        guard let url = urlCompontents?.url else {
            completion(nil, GeneralError.error(msg: "Invalid endpoint"))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        addHeaders(urlRequest: &urlRequest, headers: headers)
        doRequest(urlRequest, completion: completion)
    }
    
    func doPost(_ endpoint: String, headers: [String: String] = [:], body: Data?, completion: @escaping NetworkCompletionHandler) {
        guard let url = URL(string: endpoint) else {
            completion(nil, GeneralError.error(msg: "Invalid endpoint"))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        addHeaders(urlRequest: &urlRequest, headers: headers)
        urlRequest.httpMethod = HTTPMethod.post.rawValue
        urlRequest.httpBody = body
        doRequest(urlRequest, completion: completion)
    }
    
}
