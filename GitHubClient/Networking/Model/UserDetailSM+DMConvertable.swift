//
//  UserDetailSM.swift
//  GitHubClient
//
//  Created by Fabe on 10/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

// MARK: - UserDetailSM
struct UserDetailSM: Codable, Equatable, DMConvertable {
    
    let login: String
    let avatarURL: String?
    let name: String?
    let location: String?
    let email: String?
    
    func toDM() -> DMType {
        return UserDetailDM(userLogin: login, userAvatar: avatarURL, userName: name ?? UserDefaults.name, userLocation: location ?? UserDefaults.location, userEmail: email ?? UserDefaults.email)
    }
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarURL = "avatar_url"
        case name, location, email
    }
}
