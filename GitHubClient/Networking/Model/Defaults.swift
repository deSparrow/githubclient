//
//  Defaults.swift
//  GitHubClient
//
//  Created by Fabe on 11/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

//In real-life app it would be best to use localized strings
struct RepositoryDefaults {
    static let description = "Description not provided"
    static let language = "Not provided"
}

struct UserDefaults {
    static let name = "Name not provided"
    static let location = "Unknown location"
    static let email = "Unknown email"
}
