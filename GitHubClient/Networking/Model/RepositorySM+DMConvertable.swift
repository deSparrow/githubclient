//
//  RepositorySM.swift
//  GitHubClient
//
//  Created by Fabe on 09/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

struct RepositorySM: Codable, Equatable {
    let totalCount: Int
    let repositories: [Item]
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case repositories = "items"
    }
}

// MARK: - Item
struct Item: Codable, Equatable, DMConvertable {
    let name: String
    let owner: Owner
    let description: String?
    let stargazersCount, openIssuesCount: Int
    let language: String?
    let forksCount: Int
    
    enum CodingKeys: String, CodingKey {
        case name, owner
        case description
        case stargazersCount = "stargazers_count"
        case openIssuesCount = "open_issues_count"
        case language
        case forksCount = "forks_count"
    }
    
    func toDM() -> DMType {
        return RepositoryDM(repositoryName: name, repositoryDescription: description ?? RepositoryDefaults.description, openIssuesCount: openIssuesCount, starsCount: stargazersCount, forksCount: forksCount, language: language ?? RepositoryDefaults.language, username: owner.login)
    }
}

// MARK: - Owner
struct Owner: Codable, Equatable {
    let login: String
}
