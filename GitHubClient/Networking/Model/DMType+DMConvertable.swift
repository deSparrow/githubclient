//
//  DMType.swift
//  GitHubClient
//
//  Created by Fabe on 10/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol DMType {
    //exists only for conformance
}

protocol DMConvertable {
    func toDM() -> DMType
}
