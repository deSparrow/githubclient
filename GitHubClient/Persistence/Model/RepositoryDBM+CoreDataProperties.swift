//
//  RepositoryDBM+CoreDataProperties.swift
//  GitHubClient
//
//  Created by Fabe on 31/08/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//
//

import Foundation
import CoreData


extension RepositoryDBM {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RepositoryDBM> {
        return NSFetchRequest<RepositoryDBM>(entityName: "RepositoryDBM")
    }

    @NSManaged public var desc: String?
    @NSManaged public var forksCount: Int32
    @NSManaged public var language: String?
    @NSManaged public var name: String?
    @NSManaged public var openIssuesCount: Int32
    @NSManaged public var ownerName: String?
    @NSManaged public var starsCount: Int32
    @NSManaged public var location: String?
    @NSManaged public var address: String?
    @NSManaged public var imageUrl: String?

}
