//
//  PersistenceService.swift
//  GitHubClient
//
//  Created by Fabe on 30/08/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataManager {
    
    private init() {}
    
    static let sharedInstance = CoreDataManager()
    
    lazy var context = persistenceContainer.viewContext
    
    lazy var persistenceContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext(completion: @escaping (_ error: NSError?) -> Void) {
        if context.hasChanges {
            do {
                try context.save()
                completion(nil)
            } catch {
                let error = error as NSError
                completion(error)
            }
        }
    }
    
    func fetch<T: NSManagedObject>(_ type: T.Type) -> [T] {
        let entityName = String(describing: type)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            let objects = try context.fetch(fetchRequest) as? [T]
            return objects ?? [T]()
        } catch {
            print(error)
            return [T]()
        }
    }
}
