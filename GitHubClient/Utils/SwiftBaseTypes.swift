//
//  SwiftBaseTypes.swift
//  GitHubClient
//
//  Created by Fabe on 10/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

extension Int {
    func toString() -> String {
        return String(self)
    }
}
