//
//  CustomExtensions.swift
//  GitHubClient
//
//  Created by Fabe on 09/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func load(from url: URL?) {
        guard let url = url else {
            return
        }
        DispatchQueue.global().async { [weak self] in
            if let this = self, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    this.image = image
                }
            }
        }
    }
    
}
