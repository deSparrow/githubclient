//
//  GeneralError.swift
//  GitHubClient
//
//  Created by Fabe on 11/07/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

enum GeneralError: Error {
    case noRepositoriesForGivenOwnerError
    case unknownError
    case error(msg: String)
}
